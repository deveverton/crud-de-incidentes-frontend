import React from 'react'
import Notification from 'components/Notification'
import { connect } from 'react-redux'
import * as duck from 'store/ducks/incidents'

class Notifications extends React.Component {
  render(){

    const notifyAjaxError = function (response) {
      let errors = '';
      for(const [, value] of Object.entries(Object.values(response))){
        for(const [, message] of Object.entries(Object.values(value))){
          errors += (typeof message === 'string' ? message : message.join('<br>')) + '<br>';
        }
      }

      return {errors};
    };

    const handleRemove = () => {
      this.props.remove()
    }

    return Object.keys(this.props.error).length > 0 ? <Notification handleRemove={handleRemove} error={notifyAjaxError(this.props.error)} /> : ''
  }
}

const mapStateToProps = (state) => ({
  error: duck.getError(state),
})

const mapDispatchToProps = {
  remove: duck.removeError
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Notifications)


