import React from 'react'
import { connect } from 'react-redux'
import * as duck from 'store/ducks/incidents'
import PropTypes from 'prop-types'
import IncidentsForm from 'forms/IncidentsForm'

function ActionButtons({ isSubmitting, update, incident, closeModal, ...props }) {
  const handleSubmit = (event) => {
    const formData = new FormData(event.target);
    event.preventDefault();
    formData.set('_method', 'PUT')
    update(incident.id, formData) 
    closeModal()
  }
 
  return (
    <IncidentsForm initialValues={incident} isSubmitting={isSubmitting} onSubmit={handleSubmit} {...props} />
  )
}

ActionButtons.propTypes = {
  incident: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.object,
  ]).isRequired
}

const mapStateToProps = state => ({
  isSubmitting: duck.isSubmitting(state)
});

const mapDispatchToProps = {
  remove: duck.remove,
  update: duck.updateData
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ActionButtons) 

