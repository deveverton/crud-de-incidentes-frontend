import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import * as duck from 'store/ducks/incidents'
import Button from 'components/Button'

class DeleteButton extends React.Component {
  render() {
    const { incident, remove} = this.props
    return (
      <Button onClick={() => remove(incident.id)} buttonStyle="dark">X</Button>
    )
  }
}

DeleteButton.propTypes = {
  incident: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.object,
  ]).isRequired,
}

const mapDispatchToProps = {
  remove: duck.remove
}
  
export default connect(
  null, 
  mapDispatchToProps
)(DeleteButton)
