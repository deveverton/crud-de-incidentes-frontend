import React from 'react'
import { Provider } from 'react-redux'
import { BrowserRouter as Router } from 'react-router-dom'
import { createGlobalStyle, ThemeProvider } from 'styled-components'
import theme from 'theme'
import configureStore from 'store'

const GlobalStyle = createGlobalStyle`
  * {
    outline: 0;
  }

  body {
    margin: 0;
    padding: 0;
    font-family: ${theme.fonts.primary};
    background-color: #eff3f7;
  }
`

const store = configureStore()

export default function Wrapper (props) {
  return (
    <Provider store={store}>
      <Router>
        <ThemeProvider theme={theme}>
          <GlobalStyle />
          <div {...props} />
        </ThemeProvider>
      </Router>
    </Provider>
  )
}
