import React from 'react'
import WithModal from 'components/WithModal'
import Button from 'components/Button'
import ActionButtons from 'containers/ActionButtons'

export default function ActionButtonsContainer({ incident, remove, isSubmitting, handleSubmit, ...props }) {
  return (
    <WithModal
      width={800}
      modal={({ closeModal }) => <ActionButtons closeModal={closeModal} isSubmitting={isSubmitting} handleSubmit={handleSubmit} incident={incident} {...props} />}
    >
      {({ toggleModal }) => (
        <Button buttonStyle="warning" onClick={toggleModal}>Editar</Button>
      )}
    </WithModal>
  )
}
