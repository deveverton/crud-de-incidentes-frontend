import axios from 'axios'

const AxiosIntance = axios.create({
    baseURL: process.env.REACT_APP_API_BASE,
    headers: {
        'Access-Control-Allow-Origin': '*'
    }
});

export default AxiosIntance
