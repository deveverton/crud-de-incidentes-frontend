import AxiosIntance from 'modules/AxiosIntance'

const methods = {
  async all () {
    return AxiosIntance.get('/incidents')
  },
  
  async find(id) {
    return AxiosIntance.get(`/incidents/${id}`)
  },

  async store(data) {
    return AxiosIntance.post(`/incidents`, data, { 
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
  },

  async update(id, data) {
    return AxiosIntance.post(`/incidents/${id}`, data, { 
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
  }, 

  async remove(id) {
    return AxiosIntance.delete(`/incidents/${id}`)
  },
}

export default methods
