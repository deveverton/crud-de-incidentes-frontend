import update from 'immutability-helper'
import { Incidents } from 'store/api'

const createActionTypes = (namespace, actions) => {
  const actionTypes = {}

  actions.forEach(action => {
    actionTypes[action] = `${namespace}/${action}`
  })

  return actionTypes
}

const createReducer = (initialState, handlers) => {
  return (state = initialState, action) => {
    return handlers.hasOwnProperty(action.type)
      ? handlers[action.type](state, action)
      : state
  }
}


/**
* Action Types.
*/

export const actionTypes = createActionTypes('incidents', [
'FETCH_ALL',
'FETCH_ALL_PENDING',
'FETCH_ALL_FULFILLED',
'FETCH_ALL_REJECTED',

'FETCH_ONE',
'FETCH_ONE_PENDING',
'FETCH_ONE_FULFILLED',
'FETCH_ONE_REJECTED',

'CREATE',
'CREATE_PENDING',
'CREATE_FULFILLED',
'CREATE_REJECTED',

'UPDATE',
'UPDATE_PENDING',
'UPDATE_FULFILLED',
'UPDATE_REJECTED',

'REMOVE',
'REMOVE_PENDING',
'REMOVE_FULFILLED',
'REMOVE_REJECTED',

'REMOVE_NOTIFICATION',
'REMOVE_NOTIFICATION_FULFILLED',
])

/**
* Initial State.
*/

export const initialState = {
  isSubmitting: false,
  isLoading: true,
  incidents: [],
  error: {}
}

/**
* Reducer.
*/

export default createReducer(initialState, {
  [actionTypes.FETCH_ALL_PENDING] (state) {
    return update(state, {
      isSubmitting: { $set: true }
    })
  },

  [actionTypes.FETCH_ALL_FULFILLED] (state, { payload: { data }}) {
  return update(state, {
      isSubmitting: { $set: false },
      isLoading: { $set: false },
      incidents: { $set: data },
      error: { $set: {} }
    })
  },

  [actionTypes.FETCH_ALL_REJECTED] (state, { payload }) {
    return update(state, {
      isSubmitting: { $set: false },
      error: { $set: payload.response?.data?.errors || {} }
    })
  },




  [actionTypes.CREATE_PENDING] (state) {
    return update(state, {
      isSubmitting: { $set: true }
    })
  },

  [actionTypes.CREATE_FULFILLED] (state, { payload: { data: { data } }}) {
  return update(state, {
      isSubmitting: { $set: false },
      incidents: { 
        data: { $set: [ ...state.incidents.data, data ]}
       },
      error: { $set: {} }
    })
  },

  [actionTypes.CREATE_REJECTED] (state, { payload }) {
    return update(state, {
      isSubmitting: { $set: false },
      error: { $set: [ payload.response?.data?.errors || {} ] }
    })
  },



  [actionTypes.UPDATE_PENDING] (state) {
    return update(state, {
      isSubmitting: { $set: true }
    })
  },

  [actionTypes.UPDATE_FULFILLED] (state, { payload: { data: { data }}, meta: { id }}) {
  return update(state, {
      isSubmitting: { $set: false },
      incidents: { $apply: records => {
        return {
          data: records.data.map(r => {
            if (r.id === id) {
              return { ...data, id }
            } else {
              return r
            }
          }
        )}}
      },
      error: { $set: {} }
    })
  },

  [actionTypes.UPDATE_REJECTED] (state, { payload }) {
    return update(state, {
      isSubmitting: { $set: false },
      error: { $set: payload.response?.data?.errors || {} }
    })
  },




  [actionTypes.REMOVE_PENDING] (state) {
    return update(state, {
      isSubmitting: { $set: true }
    })
  },

  [actionTypes.REMOVE_FULFILLED] (state, { payload: { data: { data: { id } } }}) {
  return update(state, {
      isSubmitting: { $set: false },
      incidents: { $apply: records => {
        return { data: records.data.filter(r => r.id !== id)
      }}},
      error: { $set: {} }
    })
  },

  [actionTypes.REMOVE_REJECTED] (state, { payload }) {
    return update(state, {
      isSubmitting: { $set: false },
      error: { $set: payload.response?.data?.errors || {} }
    })
  },




  [actionTypes.REMOVE_NOTIFICATION_FULFILLED] (state) {
    return update(state, {
      error: { $set: {} }
    })
  },
})

/**
* Action Creators.
*/

export const store = (data) => (dispatch, getState) => {
  dispatch({
    type: actionTypes.CREATE,
    payload: Incidents.store(data),
  })
}

export const remove = (id) => (dispatch, getState) => {
  dispatch({
    type: actionTypes.REMOVE,
    payload: Incidents.remove(id),
  })
}

export const updateData = (id, data) => (dispatch, getState) => {
  dispatch({
    type: actionTypes.UPDATE,
    payload: Incidents.update(id, data),
    meta: {
      id,
    }
  })
}

export const fetchAll = () => (dispatch, getState) => {
  dispatch({
    type: actionTypes.FETCH_ALL,
    payload: Incidents.all(),
  })
}

export const removeError = () => (dispatch, getState) => {
  dispatch({
    type: actionTypes.REMOVE_NOTIFICATION,
    payload: async () => {},
  })
}
  


export const isSubmitting = state =>
  state.incidents.isSubmitting

export const getError = state =>
  state.incidents.error

export const isLoading = state =>
  state.incidents.isLoading

export const getData = state =>
  state.incidents.incidents
