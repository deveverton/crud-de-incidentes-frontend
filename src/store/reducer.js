import { combineReducers } from 'redux'
import incidents from 'store/ducks/incidents'

export default combineReducers({
  incidents
})
