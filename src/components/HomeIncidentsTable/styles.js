import styled from 'styled-components'

export const ActionButtonsContainerStyled = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  gap: 15px;
`
