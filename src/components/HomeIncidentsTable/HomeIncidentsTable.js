import React from 'react'
import PropTypes from 'prop-types'
import Table from 'components/Table'
import Badge from 'components/Badge'
import ActionButtonsContainer from 'containers/ActionButtonsContainer'
import DeleteButton from 'containers/DeleteButton'
import { ActionButtonsContainerStyled } from './styles'

export default function HomeIncidentsTable({ data }) {

  const renderCriticality = level => {
    switch(level){
      case 'high':
        return <Badge color="danger">Alto</Badge>
      case 'medium':
        return <Badge color="warning">Médio</Badge>
      default:
      case 'low':
        return <Badge color="dark">Baixo</Badge>
    }
  }

  const renderType = type => {
    switch(type){
      case 'alarm':
        return <span>Alarme</span>
      case 'incident':
        return <Badge color="danger">Incidente</Badge>
      default:
      case 'others':
        return <span>Outros</span>
    }
  }

  return (
    <Table>
      <thead>
        <tr>
          <th>Título</th>
          <th>Descrição</th>
          <th>Criticidade</th>
          <th>Tipo</th>
          <th>Status</th>
          <th>Opções</th>
        </tr>
      </thead>
      <tbody>
        {data.map(incident => {
          return (
            <tr key={incident.id}>
              <td>{ incident.title }</td>
              <td>{ incident.description }</td>
              <td>{ renderCriticality(incident.criticality) }</td>
              <td>{ renderType(incident.type) }</td>
              <td>{ incident.status === 'active' ? 'Ativo' : 'Inativo' }</td>
              <td>
                <ActionButtonsContainerStyled>
                  <ActionButtonsContainer incident={incident} />
                  <DeleteButton incident={incident} />
                </ActionButtonsContainerStyled>
              </td>
            </tr>
          )
        })}

        {!data.length && (
          <tr>
            <td colSpan="6" style={{ textAlign: 'center'}}>Não há dados</td>
          </tr>
        )}
      </tbody>
    </Table>
  )
}

HomeIncidentsTable.propTypes = {
  data: PropTypes.array.isRequired
}
