import styled from 'styled-components';
import Button from 'components/Button';
import WithModal from 'components/WithModal';
import IncidentsCreateScreen from 'screens/IncidentsCreateScreen';

export const IncidentsHeaderStyled = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;
  
export default function IncidentsHeader(){
  return (
    <IncidentsHeaderStyled>
      <h1>Lista de incidentes</h1>
      <WithModal 
        width={800} 
        modal={({ closeModal }) => <IncidentsCreateScreen closeModal={closeModal} />}
      >
        {({ toggleModal }) => (
          <Button 
            arrow
            onClick={toggleModal}
          >
            Criar incidente
          </Button>
        )}
      </WithModal>
    </IncidentsHeaderStyled>
  )
}
