import styled from 'styled-components';

const TableStyled = styled.table`
  -webkit-box-shadow: 0px 5px 12px -12px rgb(0 0 0 / 29%);
  -moz-box-shadow: 0px 5px 12px -12px rgba(0, 0, 0, 0.29);
  box-shadow: 0px 5px 12px -12px rgb(0 0 0 / 29%);
  color: #212529;
  border-collapse: collapse;

  > th {
    padding: 0.75rem;
    vertical-align: top;
    border-top: 1px solid #dee2e6;
  }

  > thead tr {
    border: none;
    padding: 30px;
    font-size: 13px;
    color: #000;
    font-weight: 400;
    text-transform: uppercase;

    vertical-align: bottom;
    border-bottom: 2px solid #dee2e6;
  }

  > tbody tr {
    background-color: #fff;
  }

  > tbody tr:nth-of-type(odd) {
    background-color: rgba(0, 0, 0, 0.05);
  }

  > thead th, tbody td {
    border: none;
    padding: 20px 50px;
    font-size: 14px;
    vertical-align: middle;
    text-align: center;
  }
`;

export default function Table({ ...props}){
  return <TableStyled {...props} />
}
