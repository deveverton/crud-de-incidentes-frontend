import React from 'react'
import PropTypes from 'prop-types'
import PageContent from 'components/PageContent'
import IncidentsHeader from 'components/IncidentsHeader'
import HomeIncidentsTable from 'components/HomeIncidentsTable'
import Card from 'components/Card'
import Notifications from 'containers/Notifications'

export default function Home({ data: { data } }) {
  return (
    <PageContent>
      <Card>
        <Notifications />
        <IncidentsHeader />
        <HomeIncidentsTable data={data} />
      </Card>
    </PageContent>
  )
}

Home.propTypes = {
  data: PropTypes.object.isRequired
}
