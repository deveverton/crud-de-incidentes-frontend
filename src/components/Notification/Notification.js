import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components';

export const NotificationStyled = styled.div`
  position: relative;
  padding: .75rem 1.25rem;
  margin-bottom: 1rem;
  border: 1px solid transparent;
  border-radius: .25rem;
  padding-right: 3.75rem;
  color: white;
  background-color: #f1556c!important;

  > button {
    position: absolute;
    top: 2px;
    right: 2px;
    font-weight: bold;
    cursor: pointer;
    z-index: 2;
    padding: .9375rem 1.25rem;
    box-shadow: none;
    background: transparent;
    box-sizing: content-box;
    width: 1em;
    height: 1em;
    padding: .25em .25em;
    color: #000;
    border: 0;
    border-radius: .25rem;
    opacity: .5;
  }
`;

export default function Notification({ handleRemove, error }) {
  return (
    <NotificationStyled>
      <div dangerouslySetInnerHTML={{__html: error.errors}}></div>
      <button type="button" onClick={() => handleRemove()}>X</button>
    </NotificationStyled>
  )
}

Notification.propTypes = {
  handleRemove: PropTypes.func.isRequired,
  error: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.object  
  ]).isRequired
}
