import styled from 'styled-components';

const CardStyled = styled.div`
  display: flex;
  flex-direction: column;
  min-width: 0;
  word-wrap: break-word;
  background-color: #fff;
  background-clip: border-box;
  border: 0 solid #e7eaed;
  border-radius: .25rem;
  box-shadow: 0 0.75rem 6rem rgb(56 65 4 / 3%);
  overflow-x: scroll;
  
  > div {
    flex: 1 1 auto;
    padding: 1.5rem 1.5rem;
  }
`;

export default function Card({ ...props}){
  return (
    <CardStyled>
      <div>
        {props.children}
      </div>
    </CardStyled> 
  )
} 
