import React from 'react';
import PropTypes from 'prop-types'
import styled from 'styled-components';

export const BadgeStyled = styled.div`
  display: inline-block;
  padding: .25em .4em;
  font-size: 75%;
  font-weight: 700;
  line-height: 1;
  color: #fff;
  text-align: center;
  white-space: nowrap;
  vertical-align: baseline;
  border-radius: .25rem;

  background-color: ${props => props.theme.bg[props.color]};
`;

export default function Badge({ color, children }){
  return (
    <BadgeStyled color={color}>{children}</BadgeStyled>
  )
}

Badge.propTypes = {
  color: PropTypes.string.isRequired,
  children: PropTypes.string.isRequired
}
