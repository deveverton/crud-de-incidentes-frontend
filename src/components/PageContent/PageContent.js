import styled from 'styled-components';

const PageContentStyled = styled.div`
  height: auto;
  margin-top: 3%;

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export default function PageContent({ children }){
  return (
    <PageContentStyled>
      {children}
    </PageContentStyled>
  )
}
