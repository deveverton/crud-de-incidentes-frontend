import styled from 'styled-components'

export const ModalContentWrapper = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  width: ${props => props.width}px;
  transform: translate(-50%, -50%);
  box-sizing: border-box;
  padding: 20px;
  margin: 30px 0;
  max-height: 100vh;

`

export const ModalContent = styled.div`
  background-color: #fff;
  border-radius: 5px;
  padding: 20px;
`

export const ButtonClose = styled.button`
  position: absolute;
  right: 20px;
  top: -10px;
  border: none;
  cursor: pointer;
  padding: 0;
  width: 20px;
  height: 20px;
  box-sizing: border-box;
  background: transparent;
`
