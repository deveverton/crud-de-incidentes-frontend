import React from 'react'
import PropTypes from 'prop-types'
import ReactModal from 'react-modal'
import Image from 'components/Image'
import { ModalContentWrapper, ModalContent, ButtonClose } from './styles'
import closeImage from 'images/close.svg'

export default function Modal ({ isOpened, onRequestClose, children, width }) {
  return (
    <ReactModal
      isOpen={isOpened}
      onRequestClose={onRequestClose}
      ariaHideApp={false}
      style={{
        overlay: {
          position: 'fixed',
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          paddingTop: 20,
          backgroundColor: 'rgba(11, 11, 11, 0.8)',
          zIndex: 1000,
          overflow: 'auto'
        },
        content: {
          border: 'none',
          padding: 0,
          position: 'static'
        }
      }}
    >
      <ModalContentWrapper width={width}>
        <ButtonClose
          type='button'
          onClick={onRequestClose}
          title='Fechar Modal'
        >
          <Image
            src={closeImage}
            width={20}
            alt='Fechar Modal'
          />
        </ButtonClose>
        <ModalContent>
          {children}
        </ModalContent>
      </ModalContentWrapper>
    </ReactModal>
  )
}

Modal.propTypes = {
  isOpened: PropTypes.bool,
  onRequestClose: PropTypes.func.isRequired,
  width: PropTypes.number
}

Modal.defaultProps = {
  isOpened: false,
  width: 500
}
