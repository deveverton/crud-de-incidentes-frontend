import React from 'react'
import styled from 'styled-components';
import PropTypes from 'prop-types'

export const ButtonStyled = styled.button`
  background: ${props => props.theme.bg[props.buttonStyle]};
  display: inline-block;
  font-weight: 400;
  color: white;
  text-align: center;
  vertical-align: middle;
  border: 1px solid transparent;
  padding: 0.375rem 0.75rem;
  font-size: 1rem;
  line-height: 1.5;
  border-radius: 0.25rem;
  cursor: pointer;

  &[disabled] {
    opacity: 0.6;
    cursor: not-allowed;
  }
 `;

export default function Button({ children, buttonStyle, ...props }){
  return (
    <ButtonStyled buttonStyle={buttonStyle} {...props}>
      {children}
      {props.arrow && ' →'}
    </ButtonStyled>
  )
}

Button.propTypes = {
  buttonStyle: PropTypes.oneOf(['success', 'danger', 'warning', 'dark']),
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired,
}

Button.defaultProps = {
  buttonStyle: 'success'
}
