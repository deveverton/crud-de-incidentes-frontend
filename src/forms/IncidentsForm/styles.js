import styled from 'styled-components'

export const RowContainer = styled.div`
  display: flex;
  flex: 1 1 auto;
  
  > div, button {
    margin-top: 15px;
    margin-left: 15px;
  }

  > div > textarea {
    width: 100% 
  }
`

export const Label = styled.label`
  font-weight: 600;
  margin-bottom: .5rem;
`

export const Input = styled.input`
  display: block;
  padding: .45rem .9rem;
  font-size: .875rem;
  font-weight: 400;
  line-height: 1.5;
  color: #343a40;
  background-color: #fff;
  background-clip: padding-box;
  border: 1px solid #ced4da;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  border-radius: .2rem;
`

export const Textarea = styled.textarea`
  display: block;
  padding: .45rem .9rem;
  font-size: .875rem;
  font-weight: 400;
  line-height: 1.5;
  color: #343a40;
  background-color: #fff;
  background-clip: padding-box;
  border: 1px solid #ced4da;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  border-radius: .2rem;
  min-height: calc(1.5em + .9rem + 2px);
`

export const Select = styled.select`
  display: block;
  width: 100%;
  min-width: 150px;
  padding: .45rem 1.9rem .45rem .9rem;
  font-size: .875rem;
  font-weight: 400;
  line-height: 1.5;
  color: #343a40;
  background-color: #fff;
  background-repeat: no-repeat;
  background-position: right .9rem center;
  background-size: 16px 12px;
  border: 1px solid #ced4da;
  border-radius: .25rem;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
`
