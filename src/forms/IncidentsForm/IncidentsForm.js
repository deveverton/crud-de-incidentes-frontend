import React from 'react'
import PropTypes from 'prop-types'
import Button from 'components/Button'

import { Label, Input, Select, Textarea, RowContainer } from './styles'

export default function IncidentsForm({ onSubmit, isSubmitting, initialValues }) {
  return (
    <form onSubmit={onSubmit}>
      <RowContainer>
        <div>
          <Label>
            Título
            <Input name="title" defaultValue={initialValues?.title} type="text" />
          </Label> 
        </div>

        <div>
          <Label>
            Criticidade
            <Select name="criticality" defaultValue={initialValues?.criticality}>
              <option value="high">Alta</option>
              <option value="medium">Média</option>
              <option value="low">Baixa</option>
            </Select>
          </Label>
        </div>

        <div>
          <Label>
            Tipo
            <Select name="type" defaultValue={initialValues?.type}>
              <option value="alarm">Alarme</option>
              <option value="incident">Incidente</option>
              <option value="others">Outros</option>
            </Select>
          </Label>
        </div>

        <div>
          <Label>
            Status
            <Select name="status" defaultValue={initialValues?.status}>
              <option value="active">Ativo</option>
              <option value="inactive">Inativo</option>
            </Select>
          </Label>
        </div>
      </RowContainer>
      
      <RowContainer>
        <div>
          <Label>
            Descrição
            <Textarea rows="4" cols="81" name="description" defaultValue={initialValues?.description} type="text" />
          </Label>
        </div>
      </RowContainer>

      <RowContainer>
        <Button disabled={isSubmitting}>Enviar</Button>
      </RowContainer>
    </form>
  )
}

IncidentsForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  isSubmitting: PropTypes.bool.isRequired,
}

IncidentsForm.defaultProps = {
  initialValues: {}
}
