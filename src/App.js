import { Route } from 'react-router-dom'
import Wrapper from 'containers/Wrapper'
import IncidentsScreen from 'screens/IncidentsScreen'

function App() {
  return (
    <Wrapper>
      <Route path="*" component={IncidentsScreen} />
    </Wrapper>
  );
}

export default App
