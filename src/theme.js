const theme = {
  fonts: {
    primary: `'Poppins', sans-serif`,

    colors: {
      default: '',
    }
  },

  bg: {
    success: 'green',
    danger: 'red',
    warning: '#f7b84b;',
    dark: '#ccc',
  },
}

export default theme
