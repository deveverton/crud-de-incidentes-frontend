export default {
  incidents: {
    all: '/incidents',
    find: '/incidents/:id',
    create: '/incidents/new',
    update: '/incidents/:id/edit',
    delete: '/incidents/:id/delete',
  }
}
