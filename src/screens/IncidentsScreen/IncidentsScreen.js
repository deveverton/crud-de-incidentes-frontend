import React from 'react'
import { connect } from 'react-redux'
import * as duck from 'store/ducks/incidents'
import Home from 'components/Home'
import PageContent from 'components/PageContent'

class IncidentsScreen extends React.Component {
  fetchAll(){
    this.props.fetchAll()
  }

  componentDidMount(){
    this.fetchAll()
  }

  render() {
    if(this.props.isLoading){
      return (
        <PageContent>
          <h1>Carregando...</h1>
        </PageContent>
      )
    }

    return (
      <Home data={this.props.data} />
    )
  }
}

const mapStateToProps = state => ({
  isLoading: duck.isLoading(state),
  data: duck.getData(state),
})

const mapDispatchToProps = {
  fetchAll: duck.fetchAll
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IncidentsScreen)
