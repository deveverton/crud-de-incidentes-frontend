import React from 'react'
import { connect } from 'react-redux'
import * as duck from 'store/ducks/incidents'
import IncidentsForm from 'forms/IncidentsForm'

class IncidentsCreateScreen extends React.Component {

  render(){
    const { closeModal, isSubmitting, store } = this.props
    
    const handleSubmit = (event) => {
      const formData = new FormData(event.target);
      event.preventDefault();
      store(formData) 
      closeModal()
    }

    return (
      <div>
        <IncidentsForm isSubmitting={isSubmitting} onSubmit={handleSubmit} />
      </div>
    )
  } 
} 

const mapStateToProps = (state) => ({
  isSubmitting: duck.isSubmitting(state)
})

const mapDispatchToProps = {
  store: duck.store
}

export default connect(
  mapStateToProps, 
  mapDispatchToProps
)(IncidentsCreateScreen)
