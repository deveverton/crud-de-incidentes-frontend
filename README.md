# CRUD de incidentes

Este projeto foi criado com CRA [Create React App](https://github.com/facebook/create-react-app).

# Requisitos para rodar a aplicação

- NodeJS
- Yarn
- Git

## Instalação

Em alguma pasta de sua preferencia, rode o comando

### `git clone git@gitlab.com:deveverton/crud-de-incidentes-frontend.git`

Na pasta do projeto, execute o comando

### `yarn install`

Após a conclusão, execute o comando

### `yarn start`

Irá executar a aplicação em modo de desenvolvimento.\
Abra [http://localhost:3000](http://localhost:3000) para visualizar no navegador.

A página irá atualizar automaticamente se fizer alguma edição.

### `yarn build`

Compila a aplicação para a versão de distribuição na pasta `build`.


obs: arquivo .env incluso
